package bech32

import (
	"encoding/base64"
	"fmt"

	"github.com/btcsuite/btcutil/bech32"
)

const (
	bech32hrp = "ssi"
)

// Convert base64 byte slice to bech32 string
func ConvertBytesToBech32(b []byte) (string, error) {
	conv, err := bech32.ConvertBits(b, 8, 5, true)
	if err != nil {
		err := fmt.Errorf("converting %v to base32: %v", b, err)
		return "", err
	}
	encoded, err := bech32.Encode(bech32hrp, conv)
	if err != nil {
		err := fmt.Errorf("encoding %v: %v", conv, err)
		return "", err
	}
	return encoded, nil
}

// Convert base64 string to bech32 string
func ConvertBase64ToBech32(s string) (string, error) {
	raw, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return "", err
	}
	encoded, err := ConvertBytesToBech32(raw)
	return encoded, err
}

// Decode bech32 string to byte slice
func DecodeBech32ToBytes(s string) ([]byte, error) {
	if _, decoded, err := bech32.Decode(s); err == nil {
		raw, err := bech32.ConvertBits(decoded, 5, 8, false)
		if err != nil {
			err := fmt.Errorf("failed to convert 5bit to 8bit: %v", err)
			return nil, err
		}
		return raw, nil
	}
	err := fmt.Errorf("failed to decode bech32 string: %s", s)
	return nil, err
}

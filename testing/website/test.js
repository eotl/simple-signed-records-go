/* test.js */
'use strict'

import { ssr, ssrIdentity, ssrFetcher } from './js/ssr.js';

const keysBad = {
    publicKey: '7JDiEvY/VKGAZZ1rK20Qv3arUDpaZySY1w+oWVdUFqA=',
    secretKey: '4SPi47ypcWJhwl6K7tmoRGcCBCzGsUpfSxnuPTwFO9XskOIS9j9UoYBlnWsrbRC/dqtQOlpnJJjXD6hZV1QWoA=='
}

const keysGood = {
    publicKey: "Ina8EYLmpDrYsFzrlnnMWKRVSnmvhu7+CvMGacDcvM8=",
    secretKey: "fDLXDejIW9iRhOQguUNZ9YDcOtPu9Hh+ru5LL3eir2AidrwRguakOtiwXOuWecxYpFVKea+G7v4K8wZpwNy8zw==",
}

const tests = [
    {
        id: '1',
        path: '/data.json',
        keypair: false,
        rule: 'none',
        behave: 'Succeeds due to not requiring signature',
        expect: '200',
    },{
        id: '2',
        path: '/toughie.png',
        keypair: '',
        rule: 'file',
        behave: 'Server cannot find unauthenticated file',
        expect: '404',
    },{
        id: '3',
        path: '/private.json',
        keypair: {},
        rule: 'file',
        behave: 'Fails due to no signature',
        expect: '401',
    },{
        id: '4',
        path: '/private.json',
        keypair: keysBad,
        rule: 'file',
        behave: 'Fails due to signature from bad key',
        expect: '403',
    },{
        id: '5',
        path: '/private.json',
        keypair: keysGood,
        rule: 'file',
        behave: 'Succeeds with signature from good key',
        expect: '200'
    },{
        id: '6',
        path: '/files/msg-1.txt',
        keypair: keysBad,
        rule: 'wildcard (*.txt)',
        behave: 'Should fail due to signature from bad key',
        expect: '403'
    },{
        id: '7',
        path: '/files/msg-1.txt',
        keypair: keysGood,
        rule: 'wildcard (*.txt)',
        behave: 'Succeeds due to signature from good key',
        expect: '200'
    },{
        id: '8',
        path: '/files/msg-2.txt',
        keypair: keysGood,
        rule: 'wildcard (*.txt)',
        behave: 'Succeeds with signature from good key',
        expect: '200'
    },{
        id: '9',
        path: '/secrets/hidden.json',
        keypair: keysGood,
        rule: 'wildcard (/*)',
        behave: 'Succeeds with signature from good key',
        expect: '200'
    },{
        id: '10',
        path: '/secrets/toughie.png',
        keypair: keysGood,
        rule: 'wildcard (/*)',
        behave: 'Succeeds with signature from good key',
        expect: '404'
    }
]

var test

var addTestRow = function(test) {
    var row = document.createElement('tr')
    row.className = 'p-2'

    // TODO need to make a utility in ssr.js for this
    let identity = ''
    if (test.keypair !== '' && test.keypair !== false && test.keypair !== null && Object.keys(test.keypair).length > 0 && test.keypair !== undefined) {
        const decoded = ssr.decodeKeypair(test.keypair)
        identity = ssr.encodeIdentityKey(decoded)
    } else {
        identity = 'No Keypair'
    }

    var col1 = document.createElement('td')
    col1.innerHTML = ' <a href="' + test.path + '">' + '#' + test.id + ' ' + test.path + '</a><br>'
    col1.innerHTML += '<small title="Identity Key" class="text-muted">' + identity + '</small>' 

    var col2 = document.createElement('td')
    col2.innerHTML = '<code>' + test.rule + '</code>'

    var col3 = document.createElement('td')
    col3.id = 'statusText-' + test.id
    col3.innerHTML = '...'

    var col4 = document.createElement('td')
    col4.id = 'status-' + test.id
    col4.innerHTML = '...'

    var col5 = document.createElement('td')
    col5.id = 'pass-' + test.id
    col5.innerHTML = '&#128336;'
    col5.title = test.behave

    row.append(col1)
    row.append(col2)
    row.append(col3)
    row.append(col4)
    row.append(col5)

    return row
}

var updateStatus = function(test, status) {
    var col4 = document.getElementById('status-' + test.id)
    col4.innerHTML = '<code>' + status + '</code>'

    var col5 = document.getElementById('pass-' + test.id)

    if (test.expect == status) {
        col5.innerHTML = '&#9989;'
    } else {
        col5.innerHTML = '&#10060;'
    }
}

var updateTest = function(test, response) {
    var col3 = document.getElementById('statusText-' + test.id)
    let res = response

    if (response.message != undefined) {
        res = response.message
    }

    col3.innerHTML = res
}

var tableResults = document.getElementById('results') 
var hostnameTest = document.getElementById('hostname-test') 
hostnameTest.innerHTML = document.baseURI

var requestTest = function(test) {
    let url = document.baseURI.slice(0, -1)

    ssrFetcher.Get(url, test.path, test.keypair).then((response) => {
        updateStatus(test, response.status)

        let contentType = response.headers.get('Content-Type')
        let res = response.body

        if (contentType == 'application/json') {
            res = response.json()
        } else if (contentType.includes('text/html')) {
            res = response.text()
        } else if (contentType.includes('text/plain')) {
            res = response.text()
        }

        return res
    })
    .then(json => {
        updateTest(test, json)
    })
}

// Run Tests
tests.forEach((test) => {  
   var row = addTestRow(test)
   tableResults.append(row)

   setTimeout(function() {
       requestTest(test)
   }, test.id * 1000)  
})

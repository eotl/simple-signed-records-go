#!/bin/sh

set -e

BIN_PATH=~/.local/bin/
WWW_PATH="$(pwd)/testing/website"
NPM_PKG="@eotl/simple-signed-records"

build_binaries() {
  echo "Building ssr"
  cd $(pwd)/cmd/ssr/
  go build

  echo "Building ssrd"
  cd ../ssrd/
  go build

  if [ "$1" = "" ]
  then
    COPY_PATH=$BIN_PATH
  else
    COPY_PATH=$1
  fi
  echo "Copying binaries to $COPY_PATH"
  cd ../../
  cp cmd/ssr/ssr $COPY_PATH
  cp cmd/ssrd/ssrd $COPY_PATH

  echo "Done!!"
}


pull_html_js() {
  # To hardcode replace with: pkg_version="0.1.5"
  pkg_version="$(curl -s https://registry.npmjs.org/@eotl/${NPM_PKG}/latest | jq -r '.version')"
  compiled_lib="https://registry.npmjs.org/${NPM_PKG}/-/${NPM_PKG}-${pkg_version}.tgz"
  
  printf "\n%s\n" "Pulling @eotl/${NPM_PKG} ${pkg_version}"
	tmpdir="$(mktemp -d -t 'ssr.XXXXXX')"
	trap 'rm -rf -- "$tmpdir"' EXIT
	tmpfile="$tmpdir/ssr.tar.gz"

  echo "Pulling the theme compiled file..."
  curl -s "$compiled_lib" > "$tmpfile"

  echo "Extracting theme files into ${WWW_PATH} ..."
	mkdir -p "$WWW_PATH"
	cd "$WWW_PATH"
	if test -x "$(which pax 2> /dev/null)"
	then
		pax -r -z -f "$tmpfile" -s '_package/dist/__'
	else
		tar -xf "$tmpfile" --strip-components 2 package/dist
	fi

  printf "\n%s\n" "Done, ${NPM_PKG} downloaded and extracted."
	cd - >> /dev/null
}


# Script start
if [ $# -le 0 ]
then
	cat <<-EOF
Usage: ${0} binaries|ui"

Options:

  binaries                      build ssr & ssrd binaries
  ui                            pulls $NPM_PKG package from NPM
	EOF
  exit 1
elif [ "$1" = "binaries" ]; then
  build_binaries $2
elif [ "$1" = "ui" ]; then
  pull_html_js
else
  echo "Usage: ${0} binaries|ui"
  exit 1
fi

exit 0

---
title: "Simple Signed Records"
description: "Authenticate HTTP requests and their data integrity with ed25519 cryptographic keys"
draft: false
lang: "English"
leafs: ["cryptography", "authentication", "verification"]
branches: "tech"
root: "core-tech"
version: "Version 0.1"
---

*Working Draft 0.1*

Simple Signed Records (SSRs) is a scheme implemented around the concept of sending cryptographically signed data from a web browser or native app to a server. A basic example of this is sending data via HTTP requests to an HTTP server. This scheme is meant to work whereby the server MAY or MAY NOT have an existing relationship or knowledge of the user sending the records. SSRs offers cryptographic integrity of data and allows for *authentication* and *verification* of data, but do not deal with *encryption* or confidentiality of data.
This specification outlines the following common aspects of dealing with [asymetric cryptographic keys](https://en.wikipedia.org/wiki/Public-key_cryptography) and assumes the reader has basic understanding of the following.

* [Key Generation](#key-generation)
* [Key Storage](#key-storage)
* [Key Exchange](#key-exchange)
* [Key Use](#key-use)
* [Key Verification](#key-verification)
* [Key Recovery](#key-recovery)

In short, SSRs offer a more decentralization friendly method to use instead of cookie (or session) based authentication traditionally used by web applications. Additionally, SSRs can be seen in a similar degree to [oauth2](https://oauth.net/2/) or [JSON Web Tokens](https://en.wikipedia.org/wiki/JSON_Web_Token) wheres as with SSRs, instead of being issued by a webservice, the keys are generated on a user's endpoint device. The lack of relying on centralized authority, of a given website, for identity issuance gives users better portability with their data and their identities.

## Key Generation

A user creates an cryptographic keypair. This keypair SHOULD be stored in a secure endpoint on users device. Suggested key storage methods will be outlined [later in this document](#key-storage). For a reference implementation, this keypair will be using [Ed25519](https://ed25519.cr.yp.to) keys. A users keypair has two parts, which when base64 encoded, produce strings like the following: 

**IdentityKey** - what is often called a `publicKey`

```
3DXKYbaNO1x55zl0V947k3SkiyzlS8yLkEmN1TFe1sQ=
```

**AccountKey** - what is often called a `privateKey`

```
7jg2314SSd45dDAQdfxx00111llasd0oclksaDd1981adadXKYbaNO1x55zl0V947k3SkXXiNzlS8yLkEmN4TVFl1sEr=
```

The users `IdentityKey` SHOULD be displayed and made easy for a user to share with other users of the same (or other implementations). Adding more human meaningful metadata such as human name, avatar, contact methods is out of scope for SSRs but this could be described and implemented in another specification.

## Key Storage

For this specification two possible implementations will be described for storing a newly generated keypair. The first implementation will be referred to as *Basic Browser* and second *Web Extension*. Both implementations generate the users keypair in a web browser using a JavScript implementation of Ed25519 like the [tweetnacl-js](https://github.com/dchest/tweetnacl-js) library. Both examples assume a user is interacting with an SSRs compatible HTTP server hosted at `example.com`

In both example implementations the user's `AccountKey` MUST NOT be sent to the server or any other user. The implementation MUST keep the `AccountKey` under the posession of the user who generated the keypair. Whoever has an `AccountKey` is allowed to perform whatever actions that key is allowed to do on a given server.

**Basic Browser**

Upon loading SSRs implementing JavaScript from `example.com` the keypair is automatically generated and the keypair is written to the web browser's [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) or [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API). Upon revisting `example.com` the user will be able to send SSRs to compatible servers. This is a less than ideal implementation as the user's AccountKey` is not protected against the following threats which MAY be communicated to the user:

- Malicious JavaScript code loaded from `example.com` that sends the user's `AccountKey` to a server
- User clearing their browser's cache will delete their `AccountKey`

An implementation like this MAY use [sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage) or not store the `AccountKey` at all. In either case the interface SHOULD communicate to the user that their account will be lost after they quit their web browser.
 
**Web Extension**

This implementation is preferred and requires a user to install a browser extension from their browser's extensions app store. Once installed, the browser extension would generate the keypair in memory and save the keys inside the browser extension using the [storage](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage) API. This implementation would offer the following security improvements over the *Basic Browser* implementation:

- Malicious JavaScripts loaded from `example.com` cannot access user's `AccountKey`
- Clearing the browsers cache will not delete a users `AccountKey`

**Hardware Device**

As is common with 2FA tokens, cryptocurrency wallets, and smartcards- an increasing number of people rely on connecting external devices (most commonly via USB) such as a [YubiKey](https://www.yubico.com), [Trezor](https://trezor.io), or [NitroKey](https://www.nitrokey.com), respectively to upgrade their security. In these cases, a users private key exists in secure environment that is very difficult and/or near impossible to access for both the intended user or malicious actors. This offers many significant advantages.

- Malicious JavaScripts loaded from `example.com` cannot access user's `AccountKey`
- Malicious code operating at root level of host computer cannot access key
- Accidental deletion or uninstall of software (webbrowser, webextension, etc...) leaves keys unaffected


## Key Exchange

For the time being, SSR only concerns itself with one way key exchange, which is user `IdentityKey` being sent to server, in this implementation via HTTP request. The format for the requests in this implementation are taken from the [Katzenpost Certificate Format](https://github.com/katzenpost/docs/blob/master/specs/certificate.rst).


## Key Use

Use of the keypair described above in creating SSRs will be illustrated by the following example which sends HTTP request to a server.
User submits web-form containing structured data. The structured data is base64 encoded and placed in the `Certified` field. The `Expiration` field stored  unix timestamp and should be set to a reasonable application specific amount. The suggested time is six months. The type of key used is declared in the `KeyType` field. From this data the cryptgraphic signature is generated and this stored in the `Payload` field along with the matching `IdentityKey` in the `Identity` field.

```
{
  "Certified": "eyJzaXplIjoibWQiLCJ3ZWlnaHQiOiJhdmVyYWdlIiwic3BlZWQiOiJuZXh0IiwicGlja3VwX25hbWUiOiIiLCJwaWNrdXBfbWV0aG9kIjoibm9uZSIsInBpY2t1cF9jb250YWN0IjoiIiwicGlja3VwX2FkZHJlc3MiOiIiLCJwaWNrdXBfYWRkcmVzczIiOiIiLCJwaWNrdXBfY2l0eSI6IkJlcmxpbiIsInBpY2t1cF9zdGF0ZSI6IkRFIiwicGlja3VwX3Bvc3RhbCI6IjEwMTc4IiwiZGVzdF9uYW1lIjoiIiwiZGVzdF9tZXRob2QiOiJub25lIiwiZGVzdF9jb250YWN0IjoiIiwiZGVzdF9hZGRyZXNzIjoiIiwiZGVzdF9hZGRyZXNzMiI6IiIsImRlc3RfY2l0eSI6IkJlcmxpbiIsImRlc3Rfc3RhdGUiOiJERSIsImRlc3RfcG9zdGFsIjoiMTAxNzgifQ==",
  "Expiration": 1603436583,
  "KeyType":"ed25519",
  "Signatures": [{
    "Identity": "3DXKYbaNO1x55zl0V947k3SkiyzlS8yLkEmN1TFe1sQ=",
    "Payload": "piRTWhrzakS3OCMLf48oXTx3fXWJ0B6LquPquRXghDYJt1VSKH+sNjc52kOX34WcUhN3uLLn29hslaWSQwPEAg=="
  }],
  "Version": 0
}
```

The server receives an HTTP request containing the JSON payload and verifies that `Identity` key included has not been `banned` by the server. The default response from SSRs sent to a server MAY be to `reject` the request if the signature comes from a key that is `unverified` or the server MAY also accept the request and mark it in some way. In any case, the server SHOULD save the `IdentityKey` for verification at a later time.

## Key Verification

SSR sees value in offering multiple ways to do key verification- each with varying degree of user-friendlyness and advantages given how an instance of SSR is configured and used.

1. `Server Verfication` - Admins can verify keys manually by marking them as `verified` in the server `keylist`

2. `Out Of Band` - A public key can be shared [out of band](https://ssd.eff.org/en/glossary/out-band-verification) between parties via a messenger app whereby the receiver of `publicKey` has the ability to mark the senders key as `verified` in the system

3. `Trust on First Use` - Systems which implement SSR can choose to enable an optional [TOFU](https://en.wikipedia.org/wiki/Trust_on_first_use) setting on keys if so desired when operating in a relatively trusted scenario. If TOFU is allowed, the system SHOULD consider upgrading a key to `verified` after a sequence of events associated with that key is completed successfully. An example being a delivery order being completed.

4. `Verification Tokens` - Tokens are pre-generated by Admins or Special Users of an instance. The tokens are single use and consist a few easy to type words which can be passed out to potential new users via an OOB channel or on paper. The new user enters token upon making their first request. Upon sending their SSR the `Token` value is checked, if it is unused, that users key is marked as `verified`


## Key Recovery
 
When creating a new keypair, applications SHOULD offer a 16 word [seed phrase](https://en.bitcoin.it/wiki/Seed_phrase) (as is popular with crypto currency wallets) to allow a user an relatively user-friendly way to regenerate their keypair on a new or re-installed device. The following is an example seed phrase:

```
rate pluming mealybug misguiding arithmetic staphylococcic
mineralogist shortbread digitalize workmanship sensualization
understaffed contacted rambler bible reticular
```

Implementations of SSR SHOULD offer a way to for users to easily type in this seed phrase, ideally with auto-suggesting of the 16 words.


## References

- [tweetnacl-js](https://github.com/dchest/tweetnacl-js) JavaScript library implementing Ed25519
- [RFC Style Guide](https://tools.ietf.org/html/rfc7322)

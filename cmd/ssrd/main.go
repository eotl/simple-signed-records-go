package main

import (
	"errors"
	"flag"
	"io/fs"
	"log"
	"os"
	"os/signal"
	"path"
	"syscall"

	"codeberg.org/eotl/simple-signed-records/server"
)

var (
	servePort    = flag.Int("l", 0, "Listening port")
	servePath    = flag.String("path", ".", "Siteroot to serve from, current directory by default")
	config       = flag.String("config", "", "Configuration file")
	authfile     = flag.String("auth", "", "Authorization file")
	serveDirlist = flag.Bool("dirlist", false, "Enable directory listing mode")
	proxyPort    = flag.Int("p", 0, "Proxy listening port, disabled by default") //
	proxyTo      = flag.String("d", "", "URL of upstream host to proxy requests for, e.g. http://127.0.0.1")
)

// override configuration items if passed by flag
func overrides(cfg *server.Config) {
	if *serveDirlist {
		cfg.Dirlist = true
	}
	if *servePort != 0 {
		cfg.HTTPPort = *servePort
	}
	if *proxyPort != 0 {
		cfg.ProxyPort = *proxyPort
	}
	if *proxyTo != "" {
		cfg.ProxyTo = *proxyTo
	}
}

func help() {
}

func main() {
	flag.Parse()
	var err error
	var p *server.ProxyService
	var s *server.HTTPService

	// default to a file inside siteroot
	if *config == "" {
		*config = path.Join(*servePath, "/.ssr/config")
	}

	// default to a file inside siteroot
	if *authfile == "" {
		*authfile = path.Join(*servePath, "/.ssr/auth")
	}

	// receive signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// load authorization
	auth := new(server.AuthFile)
	err = auth.Load(*authfile)
	if err != nil {
		log.Fatalln(err)
	}

	// load config
	cfg, err := server.LoadConfig(*config)
	if err != nil {
		// if the configuration failed to parse, exit here.
		if !errors.Is(err, fs.ErrNotExist) {
			log.Fatalln(err)
		}
		// if no config found, apply the default options
		log.Println("No configuration found. Using defaults")
		cfg = &server.Config{CORSOrigins: []string{"*"}, CORSMethods: []string{"GET", "HEAD"}}
	}

	// apply overrides
	overrides(cfg)

	// validate config
	if err := cfg.Validate(); err != nil {
		log.Fatalln(err)
	}

	// start listeners
	if cfg.HTTPPort != 0 {
		s, err = server.New(*servePath, auth, cfg)
		if err != nil {
			log.Fatalln(err)
		}
	}

	if cfg.ProxyPort != 0 {
		p, err = server.NewProxy(cfg, auth)
		if err != nil {
			log.Fatalln(err)
		}
	}

	// shutdown services upon interrupt
	<-sigs
	if s != nil {
		s.Shutdown()
		s.Wait()
	}
	if p != nil {
		p.Shutdown()
		p.Wait()
	}
}

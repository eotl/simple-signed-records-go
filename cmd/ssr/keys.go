package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
	"golang.org/x/crypto/ssh"
)

func keys() {
	if len(os.Args) != 3 {
		fmt.Println("Usage of keys:")
		fmt.Println("  file\n    path to ssh public key file with ed25519 key")
		os.Exit(1)
	}
	path := os.Args[2]
	out, err := GetSSHPubFileString(path)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	fmt.Printf("%s\t%s\n", path, out)
}

func GetSSHPubFileString(path string) (string, error) {
	sshPubFile, err := ioutil.ReadFile(path)
	if err != nil {
		err = fmt.Errorf("unable to read public ssh key:\n%v\n", err)
		return "", err
	}
	bytes, err := parseSSHPubFile(&sshPubFile)
	if err != nil {
		err = fmt.Errorf("unable to parse public key:\n%v\n", err)
		return "", err
	}

	var eddsaPubKey eddsa.PublicKey
	err = eddsaPubKey.FromBytes(*bytes)
	if err != nil {
		err = fmt.Errorf("unable to load eddsa pubkey from bytes:\n%v\n", err)
		return "", err
	}

	return eddsaPubKey.String(), nil
}

// Returns eddsa public key in []byte
func parseSSHPubFile(data *[]byte) (*[]byte, error) {
	sshPubKey, _, _, _, err := ssh.ParseAuthorizedKey(*data)
	if err != nil {
		fmt.Errorf("unable to parse ssh public key %v", err)
		return nil, err
	}
	out := sshPubKey.Marshal()[19:]
	return &out, nil
}

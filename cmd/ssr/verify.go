package main

import (
	"codeberg.org/eotl/simple-signed-records/crypto/cert"
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
	"crypto/ed25519"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
	"log"
	"os"
)

func verify(pubKeyPath string) {
	data := []byte(stdin())
	signatures, err := cert.GetSignatures(data)
	if err != nil {
		log.Fatalln("Could not get signatures:", err)
	}

	if len(signatures) != 1 {
		log.Fatalln("There must be exactly one signature")
	}

	sig := signatures[0]

	pubKey := new(eddsa.PublicKey)
	if pubKeyPath != "" {
		f, err := os.Open(pubKeyPath)
		if err != nil {
			log.Fatalln("Could not open", pubKeyPath)
		}
		defer f.Close()
		bytes, err := ioutil.ReadAll(f)
		if err != nil {
			log.Fatalln("Could not read", pubKeyPath)
		}
		key, _, _, _, err := ssh.ParseAuthorizedKey(bytes)
		if err != nil {
			log.Fatalln(err)
		}
		cKey := key.(ssh.CryptoPublicKey)
		err = pubKey.FromBytes([]byte(cKey.CryptoPublicKey().(ed25519.PublicKey)))
	} else {
		err = pubKey.FromString(sig.Identity)
	}

	if err != nil {
		log.Fatalln("Malformed Identity Key:", err)
	}

	certified, err := cert.Verify(pubKey, data)
	if err != nil {
		log.Fatalln("Request could not be verified:", err)
	}

	fmt.Printf(string(certified))
}

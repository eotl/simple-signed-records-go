package main

import (
	"bytes"
	"codeberg.org/eotl/simple-signed-records/crypto/cert"
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
	"codeberg.org/eotl/simple-signed-records/server"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/tyler-smith/go-bip39/wordlists"
	"io"
	"io/fs"
	"math"
	"math/big"
	"net/http"
	"os"
	"path"
	"runtime"
	"strings"
	"sync"
	"time"
)

type invite struct {
	phrase string
	perm   string
}

type langs map[string][]string

var l = langs{
	"cn":    wordlists.ChineseSimplified,
	"cn_tr": wordlists.ChineseTraditional,
	"cz":    wordlists.Czech,
	"en":    wordlists.English,
	"fr":    wordlists.French,
	"it":    wordlists.Italian,
	"jp":    wordlists.Japanese,
	"kr":    wordlists.Korean,
	"es":    wordlists.Spanish,
}

func makeinvites(wl []string, numWords int, numInvites int) []string {
	wl_ent := math.Log(float64(len(wl))) / math.Log(2)
	fmt.Printf("Each word represents %f bits of entropy\n", wl_ent)

	invites := make([]string, numInvites)

	wg := new(sync.WaitGroup)
	wg.Add(numInvites)
	for i := 0; i < numInvites; i++ {
		inv := &invites[i]
		go func() {
			*inv = makeinvite(wl, numWords)
			wg.Done()
		}()
	}
	wg.Wait()
	return invites
}

func makeinvite(wl []string, numWords int) string {
	wlen := big.NewInt(int64(len(wl)))
	words := make([]string, numWords)
	for i, _ := range words {
		n, err := rand.Int(rand.Reader, wlen)
		if err != nil {
			panic(err)
		}
		words[i] = wl[n.Int64()]
	}
	invite := strings.Join(words, "-")
	fmt.Printf("%s\n", invite)
	return invite
}

func writeinvites(invites []string, magic string, output string) {
	var err error
	s := make(chan string)
	wg := new(sync.WaitGroup)
	if output == "" {
		output, err = os.Getwd()
	}
	_, err = os.Stat(output)
	if errors.Is(err, fs.ErrNotExist) {
		err = os.MkdirAll(output, os.ModeDir|0700)
		if err != nil {
			panic(err)
		}
	}
	for i := 0; i < runtime.NumCPU(); i++ {
		// start a worker routine that reads from s until it is closed
		wg.Add(1)
		go func() {
			for {
				invite, ok := <-s
				if ok {
					sum := sha256.Sum256([]byte(invite))
					hash := hex.EncodeToString(sum[:])
					fname := path.Join(output, hash)
					fd, err := os.Create(fname)
					if err != nil {
						panic(err)
					}
					_, err = fmt.Fprintf(fd, magic)
					if err != nil {
						panic(err)
					}
					fd.Close()
				} else {
					wg.Done()
					return
				}
			}
		}()
	}
	for _, invite := range invites {
		s <- invite
	}
	close(s)
	wg.Wait()
}

func redeeminvite(privKeyPath, res, token string) (string, error) {
	privKey := new(eddsa.PrivateKey)
	b := *parseSSHPrivate(privKeyPath)
	if b == nil || len(b) == 0 {
		panic("wtf")
	}
	privKey.FromBytes(b)

	sum := sha256.Sum256([]byte(token))
	hash := hex.EncodeToString(sum[:])
	i := server.InviteResponse{Token: hash}
	t, err := json.Marshal(i)
	cr, err := cert.Sign(privKey, t, time.Now().Add(time.Second*60).Unix())
	if err != nil {
		return "", err
	}
	re, err := http.Post(res, "application/json", bytes.NewReader(cr))
	if err != nil {
		return "", err
	}
	t, err = io.ReadAll(re.Body)
	if err != nil {
		return "", err
	}
	return string(t), nil
}

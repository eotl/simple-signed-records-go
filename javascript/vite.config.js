import { fileURLToPath, URL } from 'node:url'
import path, { resolve } from 'path'

import { defineConfig, loadEnv } from 'vite'
import { viteStaticCopy } from 'vite-plugin-static-copy'
import { nodePolyfills } from 'vite-plugin-node-polyfills'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

export default defineConfig({
    plugins: [
        viteStaticCopy({
            targets: [
                { src: path.resolve(__dirname, 'assets/favicon.ico'), dest: '' },
            ]
        }),
        nodePolyfills({
            globals: {
                Buffer: true,
                global: true,
            },
        })
    ],
    resolve: {
        alias: [
            {
                find: '@',
                replacement: resolve(__dirname, 'src')
            }
        ],
        extensions: ['.js']
    },
    build: {
        outDir: './dist',
        emptyOutDir: true,
        sourcemap: true,
        minify: true,
        lib: {
            entry: {
                ssr: path.resolve(__dirname, 'src/index.js')
            },
            formats: ['es', 'cjs'],
        },
        rollupOptions: {
            // Don't use `input:{}` because we use lib mode (not application mode) Vite can only do one mode https://github.com/vitejs/vite/discussions/8222
            output: {
                // Provide global variables to use in the UMD build for externalized deps
                globals: {
                },
                dir: 'dist'
            },
        },
    },
    server: {
        port: 8090,
        host: '0.0.0.0',
    }
})

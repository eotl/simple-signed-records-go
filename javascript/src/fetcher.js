/* fetcher.js signs and requests SSRs with fetch */

import nacl from 'tweetnacl'
import naclUtil from 'tweetnacl-util'
import { default as ssr } from './engine'

async function Get(endpoint, path, keypairEncoded, callback) {

    const keypair = ssr.decodeKeypair(keypairEncoded)
    const url = endpoint + path

    return fetch(url, {
        method: 'HEAD',
        headers: { 'Www-Authenticate': '' }
    }).then(response => {

        let wwwAuth = response.headers.get('Www-Authenticate')
        let contentType = response.headers.get('Content-Type')
        let headers = { 'Ssr-Authenticate': '', 'Content-Type': contentType }

        if (wwwAuth != null && keypair != null) {
            let record = JSON.parse(wwwAuth)
            record.Nonce = naclUtil.encodeBase64(nacl.randomBytes(24))
            record.Path = path

            const sig = ssr.sign({ record, keypair })
            const sig64 = btoa(JSON.stringify(sig))
            headers['Ssr-Authenticate'] = sig64
        }

        return fetch(url, {
            method: 'GET',
            headers: headers 
        }).then(response => {
            return response
        }).catch(error => {
          console.error('Inner Catch', error)
        });
    }).catch(error => {
          console.error('Main Catch', error)
    })
}

async function Post(identity, endpoint, record) {
    const validSeconds = 15778800
    let secretKey = naclUtil.decodeBase64(identity.secretKey)
    let publicKey = naclUtil.decodeBase64(identity.publicKey)

    let signedData = ssr.sign({
        record,
        keypair: { secretKey, publicKey },
        validSeconds
    })

    const response = await fetch(endpoint, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(signedData)
    })
    const json = await response.json()
    if (!json.error) {
        return json
    } else {
        console.error(json.error)
    }
}

export default {
    Get,
    Post
}

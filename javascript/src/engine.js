/* Simple Signed Records Engine */

import { Buffer } from 'buffer';
import nacl from 'tweetnacl';
import naclUtil from 'tweetnacl-util';

import { bech32 } from './lib/bech32';

const generateKeypair = function() {
    const keypair = nacl.sign.keyPair()

    return keypair
}

const decodeKeypair = function(keypairEncoded) {
    let keypair = null
    if (keypairEncoded.publicKey != undefined && keypairEncoded.secretKey != undefined) { 
        keypair = {
            publicKey: naclUtil.decodeBase64(keypairEncoded.publicKey),
            secretKey: naclUtil.decodeBase64(keypairEncoded.secretKey)
        }
    }

    return keypair
}

// Decodes bech32 key (ssi1sdf...) into raw bytes for nacl
const decodeIdentityKey = function(identityKey) {
    try {
        let keyWords = bech32.decode(identityKey)
        let keyDecode = bech32.fromWords(keyWords.words)
        let keyBuffer = new ArrayBuffer(32) 
        let publicKey = new Uint8Array(keyDecode)
    
        return publicKey

    } catch (e) {
        console.error(e)
        return false;
    }
}

const encodeIdentityKey = function(keypair) {
    try {
        let keyWords = bech32.toWords(keypair.publicKey)
        let identityKey = bech32.encode('ssi', keyWords)

        return identityKey
    } catch (e) {
        console.error(e)
        return false;
    }
}

const createBytesToSign = ({ Version, Expiration, KeyType, Certified }) => {
  const payloadBytes = Buffer.from(naclUtil.decodeBase64(Certified))

  let versionBytes = Buffer.from([0, 0, 0, 0])
  let expirationBytes = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0])
  let keyTypeBytes = Buffer.from(KeyType)
  let expireyBit = Expiration

  // Convert the timestamp into a little-endian uint64 representation
  for (let i = 0; i < 8; i++) {
    expirationBytes[i] = expireyBit & 0xff
    expireyBit = expireyBit >> 8
  }

  // Pack header bytes and payload
  const headers = Buffer.concat([versionBytes, expirationBytes, keyTypeBytes])
  const bytesTotal = Buffer.concat([headers, payloadBytes])
  const bytesToSign = new Uint8Array(bytesTotal)

  return bytesToSign
}

const createSignature = (bytesToSign, keypair) => {
  const signatureBytes = nacl.sign.detached(bytesToSign, keypair.secretKey)
  const signature = naclUtil.encodeBase64(signatureBytes)

  return signature
}

const sign = ({record, keypair, validSeconds, validUntil}) => {
  const payloadBytes = Buffer.from(JSON.stringify(record))
  const certified = Buffer.from(payloadBytes).toString('base64')
  const identityKey = encodeIdentityKey(keypair)

  // Signature expires 6 months in seconds from now
  const expiry = (Math.floor(new Date().getTime() / 1000.0) + 15778800)

  // Header bytes
  let headVersion = Buffer.from([0, 0, 0, 0])
  let headExpiry  = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0])
  let headKeyType = Buffer.from("ed25519")
  let expiryBit   = expiry

  // Convert timestamp into a little-endian uint64 representation
  for (let i = 0; i< 8; i++) {
    headExpiry[i] = expiryBit & 0xFF
    expiryBit = expiryBit >> 8
  }

  // Pack data for signing
  const headers = Buffer.concat([headVersion, headExpiry, headKeyType]) 
  const bytesTotal = Buffer.concat([headers, payloadBytes]) 
  const bytesToSign = new Uint8Array(bytesTotal)

  // Sign
  const signature = createSignature(bytesToSign, keypair)

  const signedRecord = {
    Version: 0,
    Expiration: expiry,
    KeyType: 'ed25519',
    Certified: certified,
    Signatures: [{
        Identity: identityKey,
        Payload: signature,
    }],
  };

  return signedRecord
}

const addSignature = (ssr, keypair) => {
  if (verify(ssr)) {
    const bytesToSign = createBytesToSign(ssr)
    ssr.Signatures.push(createSignature(bytesToSign, keypair))

    return ssr
  }

  return undefined;
}

const verify = (record, originalPubKey) => {
  try {

    const bytesToSign = createBytesToSign(record);

    if (record.Version !== 0) {
      return false;
    }

    // TODO: currently this only checks the first signature in the Array
    // Record
    const signature = record.Signatures[0]
    const signatureBytes = naclUtil.decodeBase64(signature.Payload)

    // Key
    const publicKeyBytes = decodeIdentityKey(signature.Identity)

    // Verify Signature
    const verification = nacl.sign.detached.verify(
        bytesToSign,
        signatureBytes,
        publicKeyBytes
    )

    return verification

  } catch (e) {
    console.error(e)
    return false;
  }
}

const data = (record) => {
  if (verify(record)) {
    const dataRaw = naclUtil.decodeBase64(record.Certified)

    return {
      data: JSON.parse(Buffer.from(dataRaw).toString('utf-8')),
      identities: record.Signatures.map(s => s.Identity),
    }
  }

  return undefined;
}

function generateCert(keypair) {
    const record = {};
    const validSeconds = 15778800;
    //const secretKey = naclUtil.decodeBase64(keypair.secretKey);
    //const publicKey = bech32.decode(keypair.publicKey);
    const dataToSign = {
      record,
      keypair: keypair,
      validSeconds,
    }

    console.log('dataToSign', dataToSign)

    const signedData = sign(dataToSign)
    const cert = btoa(JSON.stringify(signedData))

    return cert 
}

export default {
    nacl,
    naclUtil,
    generateKeypair,
    decodeKeypair,
    decodeIdentityKey,
    encodeIdentityKey,
    createBytesToSign,
    sign,
    addSignature,
    verify,
    data,
    generateCert
}

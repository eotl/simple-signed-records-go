/* Simple Signed Records Identity*/

import { Buffer } from 'buffer'
import nacl from 'tweetnacl'
import { decodeBase64, encodeBase64 } from 'tweetnacl-util'

import { bech32 } from './lib/bech32'
import ssr from './engine'

let niceware

async function init() {
    niceware = (await import('niceware')).default
}

function createIdentity() {
    const seedBytes = nacl.randomBytes(32)
    const words = niceware.bytesToPassphrase(seedBytes)

    let wordsKey = new Uint8Array(words)
    const keyPair = nacl.sign.keyPair.fromSeed(seedBytes)

    const publicKey = encodeBase64(keyPair.publicKey)
    const secretKey = encodeBase64(keyPair.secretKey)

    let publicKeyWords = bech32.toWords(keyPair.publicKey)
    const identityKey = bech32.encode('ssi', publicKeyWords)

    return {
        words,
        identityKey,
        publicKey,
        secretKey
    }
}

function restoreIdentity(wordArr) {
    const wordBytes = niceware.passphraseToBytes(wordArr)
    const keyPair = nacl.sign.keyPair.fromSeed(wordBytes)
    const restoredSecretKey = encodeBase64(keyPair.secretKey)
    const restoredPublicKey = bech32.encode('ssi', keyPair.publicKey)

    return {
        restoredSecretKey,
        restoredPublicKey
    }
}

function generateInvite() {
    const invite = niceware.generatePassphrase(8)
    return invite
}

export default {
    init,
    createIdentity,
    restoreIdentity,
    generateInvite
}
